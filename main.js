import React from 'react';
import {Provider} from 'react-redux';
import {render} from 'react-dom';
import configureStore from './store';
import ExampleApp from './example-app';

// preloaded state
// ie state from server, cookie, or localstorage
const preloadedState = {};
const store = configureStore( preloadedState );

render(
    <Provider store={store}>
        <ExampleApp />
    </Provider>,
    document.getElementById('app')
);
