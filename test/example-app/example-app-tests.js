import React from 'react';
import test from 'ava';
import {shallow, mount} from 'enzyme';
import {createMockStore} from 'redux-test-utils';
import ExampleApp from '../../example-app';

test('should have an h1', function(t){
    let state = {
        test: {
            content: 'Whhhhaaat??'
        }
    };
    let Wrapper = shallow(<ExampleApp store={createMockStore(state)} />).dive();
    t.is(Wrapper.find('h1').length, 1);
});

test('h1 should contain the text yep!', function(t){
    let state = {
        test: {
            content: 'Whhhhaaat??'
        }
    };
    let Wrapper = shallow(<ExampleApp store={createMockStore(state)} />).dive();
    t.is(Wrapper.find('h1').text(), "yep!");
});
