import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from './root-reducer';
import reduxThunk from 'redux-thunk';

const configureStore = ( preloadedState ) => {
  const middlewares = [
    reduxThunk
  ];
  const enhancers = [];
  // order matters!! createStore params must be ordered: reducer, preloadedState, enhancers
  return createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(...middlewares),
      ...enhancers
    )
  );
};

export default configureStore;
