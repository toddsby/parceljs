import React,{Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import './example-app.scss';

class ExampleApp extends Component {
    componentWillMount() {
        console.log('i mounted, woot!');
        console.log('this.props.test', this.props.test);
    }

    render() {
        return (
            <div className="example__content">
                <h1>yep!</h1>
            </div>
        );

    }
}

ExampleApp.propTypes = {
    test: PropTypes.object,
    dispatch: PropTypes.func
};

let mapDispatchToProps = function(dispatch) {
    return {
        dispatch
    };
};

let mapStateToProps = function(state) {
    return {
        test: state.test
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExampleApp);
